#!/bin/bash
script_directory="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo "Script directory: $script_directory"

ONNX="$script_directory/MLP_MNIST.onnx"
DIGIT="$script_directory/digit.npy"
OUTPUT="$script_directory/output_digit.npy"
if [ ! -e "$ONNX" ] || [ ! -e "$DIGIT" ] || [ ! -e "$OUTPUT" ]
then
    virtualenv -p python3.8 "$script_directory/py3_8"
    source "$script_directory/py3_8/bin/activate"
    pip3 install --quiet -U torch torchvision --index-url https://download.pytorch.org/whl/cpu
    pip install --quiet -U onnx

    python ./MNIST_model/torch_MLP.py --epochs=15

    deactivate
    rm -r "$script_directory/py3_8"
    rm -r "$script_directory/data"
else 
    echo "$ONNX $DIGIT $OUTPUT exist."
fi

