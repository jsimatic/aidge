Aidge
=====

:Release: |version|
:Date: |today|



.. grid:: 1 2 2 3
    :margin: 4 4 0 0
    :gutter: 1

    .. grid-item-card:: :octicon:`desktop-download` Install
        :link: source/GetStarted/install
        :link-type: doc

        Find your configuration and requirements.

    .. grid-item-card:: :octicon:`table` Quick Start
        :link: source/GetStarted/quickStart
        :link-type: doc

        Build, train and deploy your first network.

    .. grid-item-card:: :octicon:`checklist` User Guide
        :link: source/UserGuide/index
        :link-type: doc

        The main hub for detailed usage explanations.

    .. grid-item-card:: :octicon:`book` API Reference
        :link: source/API/index
        :link-type: doc

        Details every modules, classes and functions.

    .. grid-item-card:: :octicon:`light-bulb` Tutorials
        :link: source/Tutorial/index
        :link-type: doc

        Learn by seeing.
    
    .. grid-item-card:: :octicon:`comment` Exchange forum
        :link: https://gitlab.eclipse.org/groups/eclipse/aidge/-/issues

        Exchange with the team and other users over features.

What is Aidge ?
---------------

AIDGE is an open source deep learning platform specialized in the design of deep neural networks intended to operate in systems constrained by power consumption or dissipation, latency, form factor (dimensions, size, etc.), and/or cost criteria.

AIDGE offers:

* an integrated approach encompassing the entire design flow, from application development to deployment: data formatting, neural network exploration, learning, testing, and optimized code generation,
* several functions to reduce the computational complexity of models and their memory requirements, most often using quantization (during or after training) and topological optimization techniques,
* compatibility with a wide range of commercially available hardware targets, offering optimized implementations for MCUs and DSPs, GPUs, FPGAs or NPUs,
* a modular design and simple abstraction layer, so features can be added and modified with ease, including the low-level implementation of calculation functions depending on the specific characteristics of the hardware being deployed (approximate calculation, specific saturated arithmetic, etc.),
* a high degree of interoperability, with support for the ONNX standard and integration with the PyTorch and Keras platforms,
* a multiparadigm approach that integrates the simulation of neuromorphic neural network models into the same platform,
* sovereignty and control of the code, as AIDGE is totally independent of other deep learning platforms.

Licence
-------

Aidge is released under the Eclipse Public License 2.0


.. toctree::
    :maxdepth: 1
    :hidden:
    
    source/UserGuide/index.rst
    source/GetStarted/index.rst
    source/API/index.rst
    source/Tutorial/index.rst
