Install
=======

Build on Linux
--------------

Install Python
^^^^^^^^^^^^^^

Each Aidge module are built independantly from one another.
To install Aidge on Linux using pip, follow those steps :

1. Create your python environnement with python >= 3.7. For example using virtualenv :

.. code-block:: bash

    virtualenv -p python3.8 py_env_aidge
    source py_env_aidge/bin/activate

2. **Optionnal**: Set the desired install path (which should be the same that you used for ``aidge_core`` and other Aidge modules):

.. code-block:: bash

    export AIDGE_INSTALL='<path_to_aidge>/install'

.. note::

    By default Aidge install its library in the ``lib/`` folder of your python env, following `PEP405 <https://peps.python.org/pep-0405/>`_ standard.

3. First build aidge_core :

.. code-block:: bash

    cd aidge/aidge_core/
    pip install . -v


4. Then build other modules (for example ``aidge_backend_cpu``, ``aidge_onnx``) :

.. code-block:: bash

    cd aidge/aidge_backend_cpu
    pip install . -v

C++ Compilation
^^^^^^^^^^^^^^^

For each modules, create two directories ``build`` and ``ìnstall``.

Then **inside** ``build`` :

.. code-block:: bash

    cmake -DCMAKE_INSTALL_PREFIX:PATH=$(path_to_install_folder) $(CMAKE PARAMETERS) $(projet_root)

    make all install


Compilation options
~~~~~~~~~~~~~~~~~~~

.. list-table::
   :header-rows: 1

   * - Option
     - Value type
     - Description
   * - *-DCMAKE_INSTALL_PREFIX:PATH*
     - ``str``
     - Path to the install folder
   * - *-DCMAKE_BUILD_TYPE*
     - ``str``
     - If ``Debug``, compile in debug mode, ``Release`` compile with highest optimisations, default= ``Release``
   * - *-DWERROR*
     - ``bool``
     - If ``ON`` show warning as error during compilation phase, default=``OFF``
   * - *-DPYBIND*
     - ``bool``
     - If ``ON`` activate python binding, default=``ON``


If you have compiled with PyBind you can find at the root of the ``build`` file the python lib ``aidge_core.cpython*.so``
