Recipies
========

Fuse Mul Add
------------

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.fuse_mul_add

    .. tab-item:: C++
        
        .. doxygenfunction:: Aidge::fuseMulAdd

Remove flatten 
--------------

.. tab-set::

    .. tab-item:: Python

        .. autofunction:: aidge_core.remove_flatten

    .. tab-item:: C++
        
        .. doxygenfunction:: Aidge::removeFlatten

