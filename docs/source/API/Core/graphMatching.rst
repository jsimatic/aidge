Graph Matching
==============


Match
-----

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.Match
            :members:
            :inherited-members:

    .. tab-item:: C++
        
        .. doxygenclass:: Aidge::Match

Node Regex
----------

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.NodeRegex
            :members:
            :inherited-members:

    .. tab-item:: C++
        
        .. doxygenclass:: Aidge::NodeRegex

Graph Regex
-----------

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.GRegex
            :members:
            :inherited-members:

    .. tab-item:: C++
        
        .. doxygenclass:: Aidge::GRegex