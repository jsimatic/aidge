Data
====

Tensor
------

A Tensor is a multi-dimensional array defined by its dimensions, its datatype and its precision.

A Tensor can be used to represent:

-	A raw input of a DNN, such as an image or a time serie;
-	A label associated to a raw input;
-	A processed input or label computed through a DNN;
-	A parameter of a DNN, such as a weight.

AIDGE can define Tensor having the following datatype and precision: 

.. list-table::
   :header-rows: 1
   
   * - Datatype
     - Precision
   * - Float
     - 64, 32, 16
   * - Int
     - 64, 32, 16, 8, 7, 6, 5, 4, 3, 2, 1
   * - UInt
     - 64, 32, 16, 8, 7, 6, 5, 4, 3, 2, 1

.. image:: /source/_static/TensorRegistry.PNG

Dataset & Dataloader
--------------------

To load Data for training or testing the DNN, AIDGE uses a Dataset and a DataLoader.

The Dataset is an abstract object to load data from a directory and create corresponding Data structures handled by AIDGE. 
The Dataset can divide the Data into 4 partitions named “Unpartitionned”, “Learn”, “Validation” and “Test”. On loading all the data is added to the “Unpartitionned” partition. Next, created Data can be distributed in the different partitions by specifying a percentage of the Data to place in each partitions (the total being 100%).  

The purpose of the DataLoader is to provide the DNN model with the input Data read by the Dataset. The DataLoader can generate a :ref:`Tensor <source/userguide/data:tensor>` Batch, which takes the form of a Tensor with a dimension added to concatenate the various input Tensor. 

For example a 2D image RGB 64x64 px have the following dimension (using CHW format): (3,64,64).  A batch size of 32 will return a Tensor of size (using NCHW format) (32,3,64,64)).

Here is a schema that sum up the interactions between Dataset, Dataloader and the models:

.. image:: /source/_static/DatabaseDataloaderInteraction.PNG

