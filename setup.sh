#!/bin/bash
set -e  # exit when any command fails

OPT_BUILD_MODE="Debug"
OPT_WITH_TESTS="OFF"
OPT_PYTHON_MODULES="n"
OPT_WITH_PYBIND="OFF"
OPT_ALL_MODULES="y"

# ordered_list is required to ensure dependencies are build in the correct order
ordered_list=("aidge_core" "aidge_backend_cpu" "aidge_onnx" "aidge_export_cpp")
# Initialize an empty array to store the directory names
available_module_list=()
module_to_compile_list=()

add_to_unique_list() {
    local arg=$1
    if [[ ! " ${module_to_compile_list[@]} " =~ " aidge_$arg " ]]; then
        module_to_compile_list+=("aidge_$arg")
    fi
}

POSITIONAL_ARGS=()

while [[ $# -gt 0 ]]; do
  case $1 in
    -p|--python)
      OPT_PYTHON_MODULES="y"
      shift # past argument
      ;;
    -b|--pybind)
      OPT_WITH_PYBIND="ON"
      shift # past argument
      ;;
    -t|--tests)
      OPT_WITH_TESTS="ON"
      shift # past argument
      ;;
    -m|--module)
      OPT_ALL_MODULES="n"
      add_to_unique_list "$2"
      shift # past argument
      shift # past value
      ;;
    --debug)
      OPT_BUILD_MODE="Debug"
      shift # past argument
      ;;
    --release)
      OPT_BUILD_MODE="Release"
      shift # past argument
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

set -- "${POSITIONAL_ARGS[@]}" # restore positional parameters

if [[ -n $1 ]]; then
    echo "Last line of file specified as non-opt/last argument:"
    tail -1 "$1"
fi

if [[ -z "${AIDGE_INSTALL}" ]]; then
	export AIDGE_INSTALL="$(pwd)/aidge/install"
fi

# get the list of available modules
while IFS= read -r -d '' dir; do
    # Get the second part of the directory name
    # Assuming directory names are in the format: aidge/aidge_<module_name>
    name="${dir#aidge/}"

    # Add the name to the array
    available_module_list+=("$name")
done < <(find aidge -maxdepth 1 -type d -name "aidge_*" -print0)

# get the list of modules to compute
if [[ "${OPT_ALL_MODULES}" == "y" ]]; then
    module_to_compile_list=("${available_module_list[@]}")
fi

ROOTPATH=$(pwd)
mkdir -p ${AIDGE_INSTALL}

for module_name in "${ordered_list[@]}"; do
    if [[ " ${module_to_compile_list[@]} " =~ " $module_name " ]]; then
        echo "Compiling module: ${module_name}..."

        if [[ "${OPT_PYTHON_MODULES}" == "y" ]]; then
            if [ -n "$(find aidge/${module_name} -maxdepth 1 -type f -name "setup.py" -print)" ]; then
                if [[ -f "aidge/${module_name}/requirements.txt" ]]; then
                    python -m pip install -r "aidge/${module_name}/requirements.txt"
                fi
                python -m pip install "aidge/${module_name}" -v
                if [[ "${OPT_WITH_TESTS}" == "ON" ]]; then
                    echo "${ROOTPATH}/aidge/${module_name}/${module_name}/unit_tests/"
                    python -m unittest discover -s ${ROOTPATH}/aidge/${module_name}/${module_name}/unit_tests/ -v -b 1> /dev/null
                fi
            fi
        else
            if [ -n "$(find aidge/${module_name} -maxdepth 1 -type f -name "CMakeLists.txt" -print)" ]; then
                mkdir -p aidge/${module_name}/build_bundle_cpp
                cd aidge/${module_name}/build_bundle_cpp/
                cmake -DCMAKE_INSTALL_PREFIX:PATH=${AIDGE_INSTALL} -DCMAKE_BUILD_TYPE=${OPT_BUILD_MODE} -DPYBIND=${OPT_WITH_PYBIND} -DTEST=${OPT_WITH_TESTS} ${@} ..
                make all install -j$(nproc)
                if [[ "$OPT_BUILD_MODE" == "Debug" ]]; then
                    ctest --output-on-failure || true
                fi
                cd ${ROOTPATH}
            fi
        fi
    else
        echo "Skip module: ${module_name}"
    fi
done
