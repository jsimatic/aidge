# Aidge

[![License-badge](https://img.shields.io/badge/License-EPL%202.0-blue.svg)](LICENSE) [![Documentation Status](https://readthedocs.org/projects/eclipse-aidge/badge/?version=latest)](https://eclipse-aidge.readthedocs.io/en/latest/?badge=latest)

The Eclipse Aidge platform is a comprehensive solution for fast and accurate Deep Neural Network (DNN) simulation and full and automated DNN-based applications building. The platform integrates database construction, data pre-processing, network building, benchmarking and hardware export to various targets. It is particularly useful for DNN design and exploration, allowing simple and fast prototyping of DNN with different topologies. It is possible to define and learn multiple network topology variations and compare the performances (in terms of recognition rate and computationnal cost) automatically. Export hardware targets include CPU, DSP and GPU with OpenMP, OpenCL, Cuda, cuDNN and TensorRT programming models as well as custom hardware IP code generation with High-Level Synthesis for FPGA and dedicated configurable DNN accelerator IP.



| Module    | Status | Coverage |
| -------- | ------- | ------- |
| [aidge_core](https://gitlab.eclipse.org/eclipse/aidge/aidge_core)  | ![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_core/badges/main/pipeline.svg?ignore_skipped=true) | ![C++ coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_core/badges/main/coverage.svg?job=coverage:ubuntu_cpp&key_text=C%2B%2B+coverage&key_width=90) ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_core/badges/main/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)    |
| [aidge_backend_cpu](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu) | ![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/badges/master/pipeline.svg?ignore_skipped=true) | ![C++ coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/badges/master/coverage.svg?job=coverage:ubuntu_cpp&key_text=C%2B%2B+coverage&key_width=90) ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/badges/master/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)     |
| [aidge_onnx](https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx)    | ![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx/badges/master/pipeline.svg?ignore_skipped=true) | ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx/badges/master/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)    |


## System requirements

- ``CMake >= 3.15``
- ``Python >= 3.7``

Each Aidge packages may add other requirements, please check them.

## Installation

### Git clone

This repository use the [git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules) feature.
For this reason when cloning this repository you need to use the ``--recursive`` option.

```bash
git clone --recursive https://gitlab.eclipse.org/eclipse/aidge/aidge.git
```

### Build on Linux using pip

Each Aidge module are built independantly from one another.
To install Aidge on Linux using pip, follow those steps :
1. Create your python environnement with python >= 3.7. For example using virtualenv :
``` bash
virtualenv -p python3.8 py_env_aidge
source py_env_aidge/bin/activate
```

2. Set the desired install path :
``` bash
export AIDGE_INSTALL = '<path_to_aidge>/install'
```

3. First build aidge_core :
``` bash
cd aidge/aidge_core/
pip install . -v
```

4. Then build other modules (for example aidge_backend_cpu, aidge_onnx) :
``` bash
cd aidge/aidge_backend_cpu
pip install . -v
```

## Docker Image

Feel free to use one of the Dockerfiles available in the [`docker`](docker) folder.

To build the image, run where your DockerFile is
```
docker build --pull --rm -f "name_of_os.Dockerfile" -t aidge:myenv .
```

Then to run a container, run
```
docker run --name mycontainer aidge:myenv
```

## Contributing

If you would like to contribute to the Aidge project, we’re happy to have your help!
Everyone is welcome to contribute code via merge requests, to file issues on Gitlab,
to help people asking for help, fix bugs that people have filed,
to add to our documentation, or to help out in any other way.
We grant commit access (which includes full rights to the issue database, such as being able to edit labels)
to people who have gained our trust and demonstrated a commitment to Aidge.

## License

Aidge has a Eclipse Public License 2.0, as found in the [LICENSE](LICENSE).
