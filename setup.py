#!/usr/bin/env python3
""" Aidge

#TODO Temporary setup (to change with stable inplace code)
"""

DOCLINES = (__doc__ or '').split("\n")

import sys
import os

# Python supported version checks
if sys.version_info[:2] < (3, 7):
    raise RuntimeError("Python version >= 3.7 required.")


CLASSIFIERS = """\
Development Status :: 2 - Pre-Alpha
"""

import pathlib

from setuptools import setup, Extension
from setuptools import find_packages
from setuptools.command.build_ext import build_ext

def get_project_version() -> str:
    aidge_root = pathlib.Path().absolute()
    version = open(aidge_root / "version.txt", "r").read().strip()
    return version


class CMakeExtension(Extension):
    def __init__(self, name):
        super().__init__(name, sources=[])

class CMakeBuild(build_ext):

    def run(self):
        self.spawn(["./setup.sh", "--pybind", "--python"])


if __name__ == '__main__':

    setup(
        name="aidge",
        version=get_project_version(),
        python_requires='>=3.7',
        description=DOCLINES[0],
        long_description_content_type="text/markdown",
        long_description="\n".join(DOCLINES[2:]),
        classifiers=[c for c in CLASSIFIERS.split('\n') if c],
        packages=find_packages(where="."),
        include_package_data=True,
        ext_modules=[CMakeExtension("aidge")],
        cmdclass={
            'build_ext': CMakeBuild,
        },
        zip_safe=False,

    )
